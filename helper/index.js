const moment = require('moment');
module.exports = (data) => {
    if (data.length === 0) return [];

    let arrayTweetsWithoutEmptys = []

    data.filter((hashtag) => {
        if (hashtag.data.statuses.length > 0) {
            arrayTweetsWithoutEmptys.push(...hashtag.data.statuses)
        }
    });

    if (arrayTweetsWithoutEmptys.length === 0) return [];

    let objTweets = {
        maça: {},
        pera: {},
        cabeloslisos: {},
        rosas: {}
    };

    arrayTweetsWithoutEmptys.forEach((tweet) => {
        const hour = _formatDateByHour(tweet.created_at);
        const tweetName = _getTweetHashTag(tweet);
        if (_checkTweetsHasHour(objTweets[tweetName], hour)) {
            return objTweets[tweetName].name = _addMoreOneToCounter(objTweets, tweetName, hour)
        }
        objTweets = _makeTweetIntoObject(objTweets, tweetName, hour);
    })
    return Object.values(objTweets)
}


_formatDateByHour = (date) => {
    return moment(date, 'ddd MMM DD HH:mm:ss ZZ YYYY').format('HH')
}

_getTweetHashTag = (tweet) => {
    return tweet.entities.hashtags[1].text;
}

_checkTweetsHasHour = (tweets, hour) => {
    return tweets['#MaisShampooSedoso'] === hour
}

_checkTweetsHasHashTag = (tweets, hashtag) => {
    return tweets.hasOwnProperty(hashtag)
}

_addMoreOneToCounter = (objTweets, hashtag, hour) => {
    const stringCounter = objTweets[hashtag].name.split(' ')[1];
    return `${hashtag} ${parseInt(stringCounter) + 1}`
}

_makeTweetIntoObject = (objTweets, tweetName, hour) => {
    return objTweets = {
        ...objTweets,
        [tweetName]: {
            ...objTweets[tweetName] = { name: `${tweetName} 1`, '#MaisShampooSedoso': hour }
        }
    }
}