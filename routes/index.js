var Twit = require('twit');
var config = require('../api/config')
var helperData = require('../helper')
module.exports = (app) => {
    app.get('/tweets/', (req, res) => {
        var T = new Twit(config)
        arrayParams = [{ q: '%23MaisShampooSedoso%20%23maça', count: 100 },
        { q: '%23MaisShampooSedoso%20%23pera', count: 100 },
        { q: '%23MaisShampooSedoso%20%23cabeloslisos', count: 100 },
        { q: '%23MaisShampooSedoso%20%23rosas', count: 100 }
        ];

        arrayPromise = [];

        arrayParams.forEach((param) => {
            arrayPromise.push(T.get('search/tweets', param));
        })
        Promise.all(arrayPromise)
            .then((result) => {
                res.status(201).send({ tweets: helperData(result) })
            })
            .catch((e) => {
                console.log(e);
            })
    })

    app.post('/tweet/', (req, res) => {
        var T = new Twit(config)

        var tweet = {
            status: req.body.tweet
        };

        T.post('statuses/update', tweet, tweeted)

        function tweeted(err, data, response) {
            if (err) return res.status(402).send({ error: 'erro ao publicar o tweet.', err})
            res.status(201).send('sucess!')
        }
    })
}