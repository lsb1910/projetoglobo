# projetoglobo

TUTORIAL:

1 - acessar o diretorio do projeto e rodar yarn para instalar os packages.
2 - acessar o arquivo api / config, e colocar as credenciais do twitter(link de auxilio: https://blog.rapidapi.com/how-to-use-the-twitter-api/#how-to-get-a-twitter-api-key)
3 - rodar nodemon app.js, para iniciar o servidor.

-Integração:
foi utilizada a biblioteca Twit, onde apenas insere as credenciais, sem OAuth.

-LÓGICA:
1- foi feito um array de promises com todas as hashtags participantes, definidas no escopo do teste.
2- após fazer o get dos dados, são filtrados os que não retornaram tweets.
3- com os dados normalizados, é feito o agrupamento.
4- percorrendo o array de tweets, verificando se aquela hashtag, já foi tweetada para aquela hora.
5- se sim, é alterado o contador(legenda do grafico), somando +1 nele.
6- se não, é adicionado o tweet para a respectiva hashtag.

-Tweet
    -Após cada tweet, é feito a request dos dados novamente, para saber o que foi enviado.